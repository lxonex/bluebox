import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RulesEngineManager } from '../models/rulesenginemanager.interface';

@Injectable({
  providedIn: 'root'
})

export class RulesEngineManagerService {
  constructor(
    private http: HttpClient
  ) { }

  getRulesEngine() {
    return this.http.get<RulesEngineManager[]>('assets/json/rulesengine.json');
  }

}
