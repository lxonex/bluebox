import { Task } from './../models/tasks.interface';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  constructor(
    private http: HttpClient
  ) { }

  getTasks() {
    return this.http.get<Task[]>('assets/json/tasks.json');
  }

}
