import { Activity } from './../models/activities.interface';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ActivitiesService {

  constructor(
    private http: HttpClient
  ) { }

  getActivities() {
    return this.http.get<Activity[]>('assets/json/activity.json');
  }

}
