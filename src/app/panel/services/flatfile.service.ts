import { ExpectedFiles } from './../models/expectedfiles.interface';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FlatFileService {

  baseurl = 'http://40.123.38.245:8082/data-service/upload/1';

  constructor(
    private http: HttpClient
  ) { }

  getExpectedFiles() {
    return this.http.get<ExpectedFiles[]>('assets/json/expectedfiles.json');
  }

  uploadFile(formdata: FormData) {
    return this.http.post(this.baseurl, formdata);
  }

}
