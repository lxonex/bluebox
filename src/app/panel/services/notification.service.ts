import { Notification } from './../models/notifications.interface';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor(
    private http: HttpClient
  ) { }

  getActivities() {
    return this.http.get<Notification[]>('assets/json/notifications.json');
  }

}
