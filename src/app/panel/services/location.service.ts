import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';

@Injectable()
export class LocationService {

    constructor(private http: HttpClient) { }

    get() {
        return this.http.get('assets/json/locations.json');
    }
}
