import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InstanceService {

	private instance = 'Development';
	
	constructor() { }
	
	setInstance(instance: string) {
		this.instance = instance;
	}

	getInstance() {
		return this.instance;
	}

}
