import { Task } from './../../../models/tasks.interface';
import { Component, OnInit } from '@angular/core';
import { TasksService } from 'src/app/panel/services/tasks.service';

@Component({
  selector: 'blx-dashboard-my-tasks',
  templateUrl: './my-tasks.component.html'
})
export class MyTasksComponent implements OnInit {

  TasksList: Task[] = [];

  constructor(
    private service: TasksService
  ) { }

  ngOnInit() {
    this.service.getTasks().subscribe(
      data => {
        this.TasksList = data;
      },
      error => {}
    );
  }

  parseType(id: number) {
    switch (id) {
      case 1:
        return 'red';
      case 2:
        return 'yellow';
      case 3:
        return 'green';
    
      default:
        break;
    }
  }

}
