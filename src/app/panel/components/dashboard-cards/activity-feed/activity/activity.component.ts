import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'blx-dashboard-activity',
  templateUrl: './activity.component.html'
})
export class ActivityComponent implements OnInit {
    @Input() type;
    @Input() time;
    @Input() data;

  constructor() { }

  ngOnInit() {
  }

}
