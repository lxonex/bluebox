import { Activity } from './../../../models/activities.interface';
import { ActivitiesService } from '../../../services/activities.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'blx-dashboard-activity-feed',
  templateUrl: './activity-feed.component.html'
})
export class ActivityFeedComponent implements OnInit {
  
  activities_list: Activity[] = [];

  constructor(
    private service: ActivitiesService
  ) { }

  ngOnInit() {
    this.service.getActivities().subscribe(
      data => {
        this.activities_list = data;
        console.log(this.activities_list);
      },
      error => {}
    );
  }

}
