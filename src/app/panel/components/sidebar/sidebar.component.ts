import { Component, OnInit, Output, Input } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'blx-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  @Input() opensidebar = true;
  @Output() toggleSidebar = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  clicktoggleSidebar() {
    this.opensidebar = !this.opensidebar;
    this.toggleSidebar.emit('1');
  }

}
