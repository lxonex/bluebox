import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NotificationsService } from '../../services/notification.service';
import { Notification } from '../../models/notifications.interface';
import { InstanceService } from '../../services/instance.service';

@Component({
  selector: 'blx-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  notifications_number = 1;
  workplace = 'Development';
  notificationsList: Notification[] = [];

  constructor(
    private notificationService: NotificationsService,
    private instanceService: InstanceService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.notificationService.getActivities().subscribe(
      data => {
        this.notificationsList = data;
        this.notifications_number = this.notificationsList.length;
      },
      error => {}
    );
  }

  setWorkplace(data: string) {
    if ( this.instanceService.getInstance() !== data ) {
      this.instanceService.setInstance(data);
      this.workplace = data;
      this.router.navigate(['/panel/']);
    }
  }

}
