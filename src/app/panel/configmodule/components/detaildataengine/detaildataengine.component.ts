import { InstanceService } from 'src/app/panel/services/instance.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'blx-detailmodules-dataengine',
  templateUrl: './detaildataengine.component.html',
})
export class DetailDataEngineComponent implements OnInit {

  instanceList = [
    { id: '', value: 'Select a instance'},
    { id: 1, value: 'Development'},
    { id: 2, value: 'Stagging'},
    { id: 3, value: 'Production'}
  ];

  bankingList = [
    { id: '', value: 'Select a banking source'},
    { id: 1, value: 'Default core banking configuration'},
    { id: 2, value: 'Client core banking DB'},
  ];

  newModuleForm: FormGroup;

  unlockCreate = false;
  unlockEdit = true;
  btnHidden = true;
  
  instance: string = '';

  constructor(
    private toastr: ToastrService,
    private instanceService: InstanceService,
  ) {}

  ngOnInit() {
    this.instance = this.instanceService.getInstance();
    this.newModuleForm = new FormGroup({
      moduleName: new FormControl({value: 'Client Team_Data_Engine_Module', disabled: true}, Validators.required),
      instance: new FormControl({value: this.instance, disabled: true}, Validators.required),
      banking: new FormControl({value: 1, disabled: true}, Validators.required),
      coredb: new FormControl({value: 'Mongo DB Dev', disabled: true}, Validators.required),
    });
  }

  editDetail(){
    if (this.unlockEdit){
      this.newModuleForm.get('moduleName').enable();
      this.newModuleForm.get('instance').enable();
      this.newModuleForm.get('banking').enable();
      this.newModuleForm.get('coredb').enable();
      this.unlockEdit = false;
      this.unlockCreate = true;
      this.btnHidden = false;
    }
  }

  validateForm() {
    if ( this.newModuleForm.valid ) {
      this.unlockCreate = true;
    } else {
      this.unlockCreate = false;
    }
  }

  submitForm() {
    if ( this.newModuleForm.valid && this.unlockCreate ) {
      this.newModuleForm.get('moduleName').disable();
      this.newModuleForm.get('instance').disable();
      this.newModuleForm.get('banking').disable();
      this.newModuleForm.get('coredb').disable();
      this.unlockEdit = true;
      this.unlockCreate = false;
      this.btnHidden = true;
      this.toastr.success('Your Data Engine module has been updated');            
    }
  }
  
}