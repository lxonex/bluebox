import { InstanceService } from 'src/app/panel/services/instance.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'blx-detailmodules-casemanagement',
  templateUrl: './detailcasemanagement.component.html',
})
export class DetailCaseManagementComponent implements OnInit {

  instanceList = [
    { id: '', value: 'Select a instance'},
    { id: 1, value: 'Developmentwe'},
    { id: 2, value: 'Stagging'},
    { id: 3, value: 'Production'}
  ];


  policeprocedureList = [
    { id: '', value: 'Select a police '},
    { id: 1, value: 'Client Team Police and procedure module default'},
  ];

  newModuleForm: FormGroup;

  unlockCreate = false;
  unlockEdit = true;
  btnHidden = true;
  btnViewCases = true;
  
  instance: string = '';

  constructor(
    private toastr: ToastrService,
    private instanceService: InstanceService,
  ) {}

  ngOnInit() {
    this.instance = this.instanceService.getInstance();
    this.newModuleForm = new FormGroup({
      moduleName: new FormControl({value: 'Client Team_Case_Management_Module', disabled: true}, Validators.required),
      moduleDescription: new FormControl({value: 'Case Management module setup with default policy and procedure', disabled: true}, Validators.required),
      instance: new FormControl({value: this.instance, disabled: true}, Validators.required),
      policeprocedure: new FormControl({value: 1, disabled: true}, Validators.required),
    });
  }

  editDetail(){
    if (this.unlockEdit){
      this.newModuleForm.get('moduleName').enable();
      this.newModuleForm.get('moduleDescription').enable();
      this.newModuleForm.get('instance').enable();
      this.newModuleForm.get('policeprocedure').enable();
      this.unlockEdit = false;
      this.unlockCreate = true;
      this.btnHidden = false;
      this.btnViewCases = false;
    }
  }

  validateForm() {
    if ( this.newModuleForm.valid ) {
      this.unlockCreate = true;
    } else {
      this.unlockCreate = false;
    }
  }

  submitForm() {
    if ( this.newModuleForm.valid && this.unlockCreate ) {
      this.newModuleForm.get('moduleName').disable();
      this.newModuleForm.get('moduleDescription').disable();
      this.newModuleForm.get('instance').disable();
      this.newModuleForm.get('policeprocedure').disable();
      this.unlockEdit = true;
      this.unlockCreate = false;
      this.btnHidden = true;
      this.btnViewCases = true;
      this.toastr.success('Your Case Management module has been updated');            
    }
  }
  
}