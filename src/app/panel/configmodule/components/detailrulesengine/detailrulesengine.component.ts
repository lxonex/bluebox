import { InstanceService } from 'src/app/panel/services/instance.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'blx-detailmodules-rulesengine',
  templateUrl: './detailrulesengine.component.html',
})
export class DetailRulesEngineComponent implements OnInit {

  instanceList = [
    { id: '', value: 'Select a instance'},
    { id: 1, value: 'Developmentwe'},
    { id: 2, value: 'Stagging'},
    { id: 3, value: 'Production'}
  ];

  typologyList = [
    { id: '', value: 'Select a typology'},
    { id: 1, value: 'Client Team Typology module default'},
  ];
  methodologyList = [
    { id: '', value: 'Select a methodology'},
    { id: 1, value: 'Client Team Typology module default'},
  ];

  newModuleForm: FormGroup;

  unlockCreate = false;
  unlockEdit = true;
  btnHidden = true;
  btnConfRules = true;
  
  instance: string = '';

  constructor(
    private toastr: ToastrService,
    private instanceService: InstanceService,
  ) {}

  ngOnInit() {
    this.instance = this.instanceService.getInstance();
    this.newModuleForm = new FormGroup({
      moduleName: new FormControl({value: 'Client Team_Rule_Engine_Module', disabled: true}, Validators.required),
      moduleDescription: new FormControl({value: 'Rule Engine module setup with default methodology and typology', disabled: true}, Validators.required),
      instance: new FormControl({value: this.instance, disabled: true}, Validators.required),
      typology: new FormControl({value: 1, disabled: true}, Validators.required),
      methodology: new FormControl({value: 1, disabled: true}, Validators.required),
    });
  }

  editDetail(){
    if (this.unlockEdit){
      this.newModuleForm.get('moduleName').enable();
      this.newModuleForm.get('moduleDescription').enable();
      this.newModuleForm.get('instance').enable();
      this.newModuleForm.get('typology').enable();
      this.newModuleForm.get('methodology').enable();
      this.unlockEdit = false;
      this.unlockCreate = true;
      this.btnHidden = false;
      this.btnConfRules = false;
    }
  }

  validateForm() {
    if ( this.newModuleForm.valid ) {
      this.unlockCreate = true;
    } else {
      this.unlockCreate = false;
    }
  }

  submitForm() {
    if ( this.newModuleForm.valid && this.unlockCreate ) {
      this.newModuleForm.get('moduleName').disable();
      this.newModuleForm.get('moduleDescription').disable();
      this.newModuleForm.get('instance').disable();
      this.newModuleForm.get('typology').disable();
      this.newModuleForm.get('methodology').disable();
      this.unlockEdit = true;
      this.unlockCreate = false;
      this.btnHidden = true;
      this.btnConfRules = true;
      this.toastr.success('Your Rules Engine module has been updated');            
    }
  }
  
}