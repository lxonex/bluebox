import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {MatProgressBarModule} from '@angular/material'
import { RulesEngineManager } from 'src/app/panel/models/rulesenginemanager.interface';
import { RulesEngineManagerService } from 'src/app/panel/services/rulesenginemanager.service';

@Component({
    selector: 'blx-configmodule-rulesengine-rulesenginemanager',
    templateUrl: './rulesenginemanager.component.html',
})
export class RulesEngineManagerComponent implements OnInit{

    listRulesEngines: RulesEngineManager[];

    constructor(
        private service: RulesEngineManagerService,
        private tastr: ToastrService
    ){}
    ngOnInit(){
        this.service.getRulesEngine().subscribe(data => {
            this.listRulesEngines = data;
        })
    }

    transformProgress(valor: number){
        return Math.round(valor*100/6);
    }

    editItem(item: RulesEngineManager){
        console.log(item);
    }

    exportItem(item: RulesEngineManager){
        console.log(item);
    }

    assignItem(item: RulesEngineManager){
        console.log(item);
    }
}