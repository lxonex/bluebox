import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbProgressbar, NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSelectModule } from '@angular/material/select';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigModuleRoutingModule } from './configmodule-routing.module';
import { ConfigModuleComponent } from './configmodule.component';
import { DetailDataEngineComponent } from './components/detaildataengine/detaildataengine.component';
import { DetailRulesEngineComponent } from './components/detailrulesengine/detailrulesengine.component';
import { RulesEngineManagerComponent } from './components/detailrulesengine/subcomponent/rulesenginemanager/rulesenginemanager.component';
import { MatProgressBarModule } from '@angular/material';
import { DetailCaseManagementComponent } from './components/detailcasemanagement/detailcasemanagement.component';


const COMPONENTS = [
  ConfigModuleComponent,
  DetailRulesEngineComponent,
  DetailDataEngineComponent,
  DetailCaseManagementComponent,
  RulesEngineManagerComponent,
];

const MODULES = [
	ConfigModuleRoutingModule,
	CommonModule,
  NgbModule,
  FormsModule,
  ReactiveFormsModule,
  MatInputModule,
  MatSelectModule,
  MatProgressBarModule,
];

const SERVICES = [
];

@NgModule({
  declarations: [
    ...COMPONENTS,
  ],
  imports: [
    ...MODULES,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class ConfigModuleModule { }
