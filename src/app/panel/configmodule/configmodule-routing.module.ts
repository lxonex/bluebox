
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigModuleComponent } from './configmodule.component';
import { DetailDataEngineComponent } from './components/detaildataengine/detaildataengine.component';
import { DetailRulesEngineComponent } from './components/detailrulesengine/detailrulesengine.component';
import { RulesEngineManagerComponent } from './components/detailrulesengine/subcomponent/rulesenginemanager/rulesenginemanager.component';
import { DetailCaseManagementComponent } from './components/detailcasemanagement/detailcasemanagement.component';


const routes: Routes = [{
  path: '',
  component: ConfigModuleComponent,
  children: [
    {
      path: 'dataengine',
      component: DetailDataEngineComponent,
    },
    {
      path: 'rulesengine',
      component: DetailRulesEngineComponent,
    },
    {
      path: 'rulesengine/manager',
      component: RulesEngineManagerComponent,
    },
    {
      path: 'casemanagement',
      component: DetailCaseManagementComponent,
    },
    {
      path: '',
      redirectTo: 'add',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigModuleRoutingModule { }
