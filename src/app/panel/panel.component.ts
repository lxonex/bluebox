import { Component } from '@angular/core';

@Component({
  selector: 'blx-panel',
  templateUrl: './panel.component.html',
})
export class PanelComponent {
  opensidebar = true;

  toggleSidebar() {
    this.opensidebar = !this.opensidebar;
  }
}

