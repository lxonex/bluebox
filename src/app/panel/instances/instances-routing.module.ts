import { AddInstanceComponent } from './components/addinstance/addinstance.component';
import { InstancesComponent } from './instances.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewInstanceComponent } from './components/viewinstance/viewinstance.component';


const routes: Routes = [{
  path: '',
  component: InstancesComponent,
  children: [
    {
      path: 'view',
      component: ViewInstanceComponent,
    },
    {
      path: 'add',
      component: AddInstanceComponent,
    },
    {
      path: '',
      redirectTo: 'add',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InstancesRoutingModule { }
