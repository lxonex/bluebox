import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddInstanceComponent } from './components/addinstance/addinstance.component';
import { ViewInstanceComponent } from './components/viewinstance/viewinstance.component';
import { InstancesRoutingModule } from './instances-routing.module';
import { NgbModule, NgbProgressbar, NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import {MatSelectModule} from '@angular/material/select';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InstancesComponent } from './instances.component';
import { DirTreeComponent } from './components/addinstance/subcomponent/dirtree/dirtree.component';
import {MatProgressBarModule, MatIconModule} from '@angular/material'
import { CdkTreeModule } from '@angular/cdk/tree';

const COMPONENTS = [
  InstancesComponent,
  AddInstanceComponent,
  ViewInstanceComponent,
  DirTreeComponent,
];

const MODULES = [
    InstancesRoutingModule,
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    MatProgressBarModule,
    CdkTreeModule,
    MatIconModule,

];

const SERVICES = [
];

@NgModule({
  declarations: [
    ...COMPONENTS,
  ],
  imports: [
    ...MODULES,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class InstancesModule { }
