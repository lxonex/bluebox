import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import {Router} from "@angular/router"

@Component({
    selector: 'blx-newinstances-addinstance',
    templateUrl: './addinstance.component.html',
})
export class AddInstanceComponent implements OnInit{
    newInstanceForm: FormGroup;
    unlockCreate = false;
    showtree = false;
    treeList = [
      { id: '', value: 'Select a directory'},
      { id: 1, value: 'My directory'},
    ];
    folderName = '';
    constructor(        
        private toastr: ToastrService,
        private router: Router,
    ){}
    ngOnInit(){
        this.newInstanceForm = new FormGroup({
            instanceName: new FormControl('', Validators.required),
            dirtreeName: new FormControl('', Validators.required),
          });
    }

    validateForm() {
      if ( this.newInstanceForm.valid && this.folderName !== '') {
          this.unlockCreate = true;
      } else {
          this.unlockCreate = false;
      }
    }
    
    submitForm() {
      if ( this.newInstanceForm.valid && this.unlockCreate ) {
        this.router.navigate(['/panel/instances/view']);
        this.toastr.success('Your data module has been created succesfully');            
      }
    }
    showdirtree(){
        console.log("click");
      this.showtree=true;
    }
    onFolderChange(event) {
      this.folderName = event;
      this.validateForm();
    }
}