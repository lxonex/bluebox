import { FlatNode } from './../../../../../models/flatnode.interface';
import { LocationService } from './../../../../../services/location.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { ArrayDataSource } from '@angular/cdk/collections';

@Component({
  selector: 'blx-newinstances-instance-dirtree',
  templateUrl: './dirtree.component.html',
})
export class DirTreeComponent implements OnInit {
  
  @Output() folderChange = new EventEmitter();

  instancesData = [];

  treeControl = new NestedTreeControl<FlatNode> (node => node.children);
  dataSource = new ArrayDataSource(this.instancesData);

  selectedInstanceDirectory = '';

  hasChild = (_: number, node: FlatNode) => !!node.children && node.children.length > 0;


  constructor(
    private service: LocationService
  ) {}

  ngOnInit() {
    this.service.get().subscribe(
      data => {
        //this.dataSource.data = JSON.parse(JSON.stringify(data));
        this.instancesData = JSON.parse(JSON.stringify(data));
        this.dataSource = new ArrayDataSource(this.instancesData);
        this.treeControl = new NestedTreeControl<FlatNode> (node => node.children);
        console.log(this.instancesData);
      },
      error => {
        console.log(error);
      }
    );

  }

  setSelected(name) {
    this.selectedInstanceDirectory = name;
    this.folderChange.emit(name);
  }

  
}