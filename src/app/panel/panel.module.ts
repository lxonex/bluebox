import { LocationService } from './services/location.service';
import { FlatFileService } from './services/flatfile.service';
import { NotificationsService } from './services/notification.service';
import { ActivitiesService } from './services/activities.service';
import { ActivityComponent } from './components/dashboard-cards/activity-feed/activity/activity.component';
import { MyTasksComponent } from './components/dashboard-cards/my-tasks/my-tasks.component';
import { ActivityFeedComponent } from './components/dashboard-cards/activity-feed/activity-feed.component';
import { NgbModule, NgbProgressbar, NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import { PanelComponent } from './panel.component';
import { PanelRoutingModule } from './panel-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { TasksService } from './services/tasks.service';
import { MatInputModule } from '@angular/material/input';
import { SharedModule } from '../shared/shared.module';

const COMPONENTS = [
  NavbarComponent,
  SidebarComponent,
  DashboardComponent,
  PanelComponent,
  ActivityFeedComponent,
  MyTasksComponent,
  ActivityComponent
];

const MODULES = [
  PanelRoutingModule,
  CommonModule,
  NgbModule,
  NgbProgressbarModule,
  MatInputModule,
  SharedModule
];

const SERVICES = [
  ActivitiesService,
  TasksService,
  NotificationsService,
  FlatFileService,
  LocationService
];

@NgModule({
  declarations: [
    ...COMPONENTS,
  ],
  imports: [
    ...MODULES,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class PanelModule { }
