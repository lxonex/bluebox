import { NewDataEngineComponent } from './components/newdataengine/newdataengine.component';
import { NewModulesViewComponent } from './components/newmodulesview/newmodulesview.component';
import { NewModulesComponent } from './newmodules.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [{
  path: '',
  component: NewModulesComponent,
  children: [
    {
      path: 'add',
      component: NewModulesViewComponent,
    },
    {
      path: 'newengine',
      component: NewDataEngineComponent,
    },
    {
      path: '',
      redirectTo: 'add',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewModulesRoutingModule { }
