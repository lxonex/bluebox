import { BankingDBComponent } from './components/newdataengine/subcomponents/bankingdb/bankingdb.component';
import { FlatFileComponent } from './components/newdataengine/subcomponents/flatfile/flatfile.component';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewDataEngineComponent } from './components/newdataengine/newdataengine.component';
import { NewModulesViewComponent } from './components/newmodulesview/newmodulesview.component';
import { NewModulesComponent } from './newmodules.component';
import { NewModulesRoutingModule } from './newmodules-routing.module';
import { NgbModule, NgbProgressbar, NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import {MatSelectModule} from '@angular/material/select';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


const COMPONENTS = [
  NewModulesComponent,
  NewModulesViewComponent,
  NewDataEngineComponent,
  FlatFileComponent,
  BankingDBComponent
];

const MODULES = [
	NewModulesRoutingModule,
	CommonModule,
  NgbModule,
  FormsModule,
  ReactiveFormsModule,
  MatInputModule,
  MatSelectModule
];

const SERVICES = [
];

@NgModule({
  declarations: [
    ...COMPONENTS,
  ],
  imports: [
    ...MODULES,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class NewModulesModule { }
