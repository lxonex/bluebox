import { ExpectedFiles } from './../../../../../models/expectedfiles.interface';
import { FlatFileService } from './../../../../../services/flatfile.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'blx-newmodules-dataengine-flatfile',
  templateUrl: './flatfile.component.html',
})
export class FlatFileComponent implements OnInit {

  @Output() doneEvent = new EventEmitter();;

  @ViewChild('file', {static: false}) file;
  public files = [];

  loadCard = false;

  //status:
  // 1: green -- A single correct file type and > 0 byte size has been selected that matches the expected file name
  // 2: yellow -- A file with the correct file name was selected, and the file type and/or the file size is incorrect. However, the file was 'trashed', and is no longer in the table.
  // 3: red -- A file with the correct file name was selected, and the file type and/or the file size is incorrect. The user has NOT trashed the file, and is still in the table list.
  // 4: grey -- No file has been selected for this fileName, and is the starting color for all fileName

  expectedFilesList: ExpectedFiles[] = [
    {
      status: 4,
      filename: 'Customer_individual',
      description: 'Customer data for individuals'
    },
    {
      status: 4,
      filename: 'Account_general',
      description : 'Account data for general types of accounts'
    },
    {
      status: 4,
      filename: 'Transaction_general',
      description: 'Transactional data for general accounts'
    },
    {
      status: 4,
      filename: 'Customer_entity',
      description: 'Customer data for legal entities'
    },
    {
      status: 4,
      filename: 'Account_loan',
      description: 'Account data for loan account types'
    },    
    {
      status: 4,
      filename: 'Transaction_loan',
      description: 'Transactional data for loan accounts'
    }
  ];

  statusList = [
    { value: 1, desc: 'not uploaded' },
    { value: 2, desc: 'uploading' },
    { value: 3, desc: 'success' },
    { value: 4, desc: 'error' },
  ];

  unlockDone = false;

  constructor(
    private service: FlatFileService,
    private toastr: ToastrService
  ) {} 
  

  ngOnInit() { }

  addFiles() {
    this.file.nativeElement.click();
  }

  onFilesAdded() {
    const files: { [key: string]: File } = this.file.nativeElement.files;
    for (let key in files) {
      if (!isNaN(parseInt(key))) {
        this.files.push( {
          name: files[key].name,
          status: this.statusList[0].desc,
          statuscode: 0,
          size: this.getSize(files[key].size),
          processtatus: '',
          timestamp: new Date(),
          file: files[key]
        } );
      }
    }
    this.file.nativeElement.value = '';
    this.sortArray();
    this.unlockDone = true;
  }

  sortArray() {
    this.files.sort( (a, b) => {
      if ( a.timestamp < b.timestamp ) {
        return 1;
      }
      if ( a.timestamp > b.timestamp ) {
        return -1;
      }
      return 0;
    } );
  }

  deleteItem(item) {
    const aux_name = this.expectedFilesList.filter( f => item.name.indexOf(f.filename) !== -1 );
    if ( aux_name.length > 0 ) {
      if ( item.statuscode == 200 ) {
        aux_name[0].status = 2;
      }
    }
    this.files = this.files.filter( i => i !== item );
    this.verifyFiles();
  }

  getSize(size: number) {
    return (size / 1024).toFixed(2) + 'k';
  }

  verifyFiles() {
    if ( this.files.length > 0 ) {
      let numFilesReady = 0;

      let hasError = false;

      for ( const file of this.files ) {
        if ( file.statuscode == 0 ) {
          numFilesReady = numFilesReady + 1;
        }
        const aux_name = this.expectedFilesList.filter( f => file.name.indexOf(f.filename) !== -1 );
        if ( aux_name.length > 0 ) {
          if ( file.statuscode == 200 ) {
            aux_name[0].status = 1;
          }
          if ( file.statuscode !== 200 ) {
            aux_name[0].status = 3;
          }
        }
        if ( file.statuscode !== 200 ) {
          hasError = true;
          this.doneEvent.emit(false);
        }
      }

      if ( numFilesReady > 0 ) {
        this.unlockDone = true;
      } else {
        this.unlockDone = false;
      }
    } else {
      this.unlockDone = false;
    }
  }

  async doneClick() {
    if (this.files.length > 0 && this.unlockDone) {
      this.unlockDone = false;
      for ( const obj of this.files ) {
        if ( obj.statuscode == 0 ) {
          const formdata = new FormData();
          formdata.append('content', obj.file, obj.file.name);
          obj.status = this.statusList[1].desc;
          await this.service.uploadFile(formdata).toPromise().then(
            data => {
              if ( JSON.parse(JSON.stringify(data)).header.code == 200 ) {
                obj.status = this.statusList[2].desc;
                obj.statuscode = 200;
                obj.processtatus = JSON.parse(JSON.stringify(data)).payload[0].status;
              }
            }
          ).catch(
            error => {
              console.log('error');
              obj.status = error.error.header.details;
              obj.statuscode = 400;
            }
          );
        }
      }
      this.verifyFiles();
      this.doneEvent.emit(true);
    } else {
      this.toastr.error('There are no files added', '');
    }
  }
}