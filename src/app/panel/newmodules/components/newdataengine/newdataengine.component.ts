import { InstanceService } from 'src/app/panel/services/instance.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'blx-newmodules-dataengine',
  templateUrl: './newdataengine.component.html',
})
export class NewDataEngineComponent implements OnInit {

  instanceList = [
    { id: '', value: 'Select a instance'},
    { id: 1, value: 'Development'},
    { id: 2, value: 'Stagging'},
    { id: 3, value: 'Production'}
  ];

  bankingList = [
    { id: '', value: 'Select a banking source'},
    { id: 1, value: 'Flat file'},
    { id: 2, value: 'Client core banking DB'},
  ];

  newModuleForm: FormGroup;

  showFlatFileComponent = false;
  showClientCoreBankingDBComponent = false;

  flatfileIsDone = false;

  unlockCreate = false;

  instance: string = '';

  constructor(
    private toastr: ToastrService,
    private instanceService: InstanceService,
  ) {}

  ngOnInit() {
    this.instance = this.instanceService.getInstance();
    this.newModuleForm = new FormGroup({
      moduleName: new FormControl('', Validators.required),
      instance: new FormControl({value: this.instance, disabled: true}, Validators.required),
      banking: new FormControl('', Validators.required),
      coredb: new FormControl({value: 'Mongo DB Dev', disabled: true}, Validators.required),
    });
  }

  bankingSourceChange() {
    this.unlockCreate = false;
    if ( this.newModuleForm.get('banking').value == 1 ) {
      this.showFlatFileComponent = true;
    } else {
      this.showFlatFileComponent = false;
    }
    if ( this.newModuleForm.get('banking').value == 2 ) {
      this.showClientCoreBankingDBComponent = true;
      this.validateForm();
    } else {
      this.showClientCoreBankingDBComponent = false;
    }
  }

  validateForm() {
    if ( this.newModuleForm.valid ) {
      if ( this.newModuleForm.get('banking').value == 1 ) {
        if ( this.flatfileIsDone ) {
          this.unlockCreate = true;
        } else {
          this.unlockCreate = false;
        }
      }
      if ( this.newModuleForm.get('banking').value == 2 ) {
        this.unlockCreate = true;        
      }
    } else {
      this.unlockCreate = false;
    }
  }

  submitForm() {
    if ( this.newModuleForm.valid && this.unlockCreate ) {
      this.toastr.success('Your data module has been created succesfully');            
    }
  }

  doneFlatFile(event) {
    if ( event ) {
      if ( this.newModuleForm.get('banking').value == 1 ) {
        this.flatfileIsDone = true;
      }
      if ( this.newModuleForm.get('banking').value == 2 ) {
      }
      if ( this.newModuleForm.valid) {
        this.unlockCreate = true;
      } else {
        this.unlockCreate = false;
      }
    } else {
      this.unlockCreate = false;
      if ( this.newModuleForm.get('banking').value == 1 ) {
        this.flatfileIsDone = false;
      }
      if ( this.newModuleForm.get('banking').value == 2 ) {
      }
    }
  }
  
}