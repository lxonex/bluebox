import { PanelComponent } from './panel.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';


const routes: Routes = [{
  path: '',
  component: PanelComponent,
  children: [
    {
        path: 'dashboard',
        component: DashboardComponent,
    },
    {
      path: 'newmodule',
      loadChildren: () => import('./newmodules/newmodules.module').then(m => m.NewModulesModule),
    },
    {
      path: 'instances',
      loadChildren: () => import('./instances/instances.module').then(m => m.InstancesModule),
    },
    {
      path: 'configmodule',
      loadChildren: () => import('./configmodule/configmodule.module').then(m => m.ConfigModuleModule),
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PanelRoutingModule { }
