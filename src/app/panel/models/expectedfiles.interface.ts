export interface ExpectedFiles {
    status: number;
    filename: string;
    description: string;
}