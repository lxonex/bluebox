export interface Notification {
    id: number,
    type: number,
    title: string,
    time: string
}