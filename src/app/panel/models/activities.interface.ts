export interface Activity {
    id: number;
    type: number;
    data?: any;
    time: string;
}