export interface Task {
    id: number,
    title: string,
    percentage: number,
    type: number
}