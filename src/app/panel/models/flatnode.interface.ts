export interface FlatNode {
    name: string;
    children?: FlatNode[];
    level: number;
}
