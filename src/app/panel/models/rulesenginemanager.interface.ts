export interface RulesEngineManager{
    id: number;
    name: string;
    status: number;
    progress: number;
    last_updated_user: string;
    last_updated_date: string;
}
