import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'blx-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  @Input() modalController;

  constructor(
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.registerForm = new FormGroup({
      name: new FormControl('', Validators.required),
      surname: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
      type: new FormControl('', Validators.required)
    });
  }

  register() {
    if ( this.registerForm.valid ) {
      this.modalController.close();
    } else {
      if (this.registerForm.get('name').errors && this.registerForm.get('name').errors.required) {
        this.toastr.error('Name is required', '');
      }
      if (this.registerForm.get('surname').errors && this.registerForm.get('surname').errors.required) {
        this.toastr.error('Lastname is required', '');
      }
      if (this.registerForm.get('email').errors && this.registerForm.get('email').errors.required) {
        this.toastr.error('Email is required', '');
      }
      if (this.registerForm.get('email').errors && this.registerForm.get('email').errors.email) {
        this.toastr.error('Email is not valid', '');
      }
      if (this.registerForm.get('password').errors && this.registerForm.get('password').errors.required) {
        this.toastr.error('Password is required', '');
      }
      if (this.registerForm.get('type').errors && this.registerForm.get('type').errors.required) {
        this.toastr.error('Type is required', '');
      }
    }
  }

}
