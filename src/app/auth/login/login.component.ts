import { Component, OnInit, Input } from '@angular/core';
import { NgxSmartModalComponent, NgxSmartModalService } from 'ngx-smart-modal';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  errorList = [];
  typePassword = 'password';
  isshowingPassword = false;
  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
      remember: new FormControl(false),
    });
  }

  login() {
    if (this.loginForm.valid) {
      //login logic
      this.router.navigate(['/panel']);
    }  
  }

  showPassword() {
    if ( this.isshowingPassword ) {
      this.typePassword = 'password';
      this.isshowingPassword = false;    
    } else {
      this.typePassword = 'text';
      this.isshowingPassword = true;
    }
  }

}
