import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AuthComponent } from './auth.component';
import { AuthRoutingModule } from './auth-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { ToastrModule } from 'ngx-toastr';

const COMPONENTS = [
  LoginComponent,
  RegisterComponent,
  ForgotpasswordComponent,
  AuthComponent
];

const MODULES = [
  AuthRoutingModule,
  CommonModule,
  ReactiveFormsModule,
  FormsModule,
  NgxSmartModalModule.forRoot(),
  MatInputModule
];

const SERVICES = [

];

@NgModule({
  declarations: [
    ...COMPONENTS,
  ],
  imports: [
    ...MODULES,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class AuthModule { }
