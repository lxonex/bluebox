import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'blx-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  resetForm: FormGroup;
  @Input() modalController;

  constructor(
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.resetForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email])
    });
  }

  resetPassword() {
    if (this.resetForm.valid) {
      this.modalController.close();

    } else {
      if (this.resetForm.get('email').errors && this.resetForm.get('email').errors.required) {
        this.toastr.error('Email is required', '');
      }
      if (this.resetForm.get('email').errors && this.resetForm.get('email').errors.email) {
        this.toastr.error('Email is not valid', '');
      }
    }
  }

}
